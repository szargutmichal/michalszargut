<?php
/**
 * Created by PhpStorm.
 * User: szarg
 * Date: 21.02.2019
 * Time: 16:02
 */

namespace MichalSzargut\Task\Cron;

use MichalSzargut\Task\Helper\ConfigData;
use MichalSzargut\Task\Helper\CsvFile;
use MichalSzargut\Task\Helper\FileDownloader;
use MichalSzargut\Task\Helper\Product\SimpleProduct;
use MichalSzargut\Task\Logger\Logger;

class CreateOrUpdateSimpleProducts
{
    const CSV_URL = 'csv-url';
    /**
     * @var ConfigData
     */
    private $configData;
    /**
     * @var FileDownloader
     */
    private $fileDownloader;
    /**
     * @var CsvFile
     */
    private $csvFileHelper;
    /**
     * @var SimpleProduct
     */
    private $simpleProduct;
    /**
     * @var Logger
     */
    private $logger;

    public function __construct(
        ConfigData $configData,
        FileDownloader $fileDownloader,
        CsvFile $csvFileHelper,
        SimpleProduct $simpleProduct,
        Logger $logger
    )
    {
        $this->configData = $configData;
        $this->fileDownloader = $fileDownloader;
        $this->csvFileHelper = $csvFileHelper;
        $this->simpleProduct = $simpleProduct;
        $this->logger = $logger;
    }

    public function execute(){
        // Getting url passed from command line or trying get url value from admin config
        $csvUrl = $this->configData->getGeneralConfig('csv_file_url');
        if (!$csvUrl) {
            $this->logger->customError('[MICHALSZARGUT_TASK_CRON_DOWNLOAD_FILE]',
                new \Exception('CSV url is not defined'));
            return;
        }
        // Trying to download the file from passed url
        $csvPath = $this->fileDownloader->downloadFile($csvUrl);
        if (!$csvPath) {
            return;
        }
        // Trying to read downloaded file and convert his content to php array
        $convertedCsvProductsData = $this->csvFileHelper->readCsvFile($csvPath);
        if (!$convertedCsvProductsData) {
            $this->logger->customError('[MICHALSZARGUT_TASK_CRON_READ_FILE]',
                new \Exception("Nothing to work with. Downloaded file is empty or has error. Please check log files"));
            return;
        }
        // Trying to process converted products data
        $this->simpleProduct->createOrUpdateProducts($convertedCsvProductsData);
        // Removing file from local storage
        if (!$this->csvFileHelper->removeCsvFile($csvPath))
            $this->logger->customError('[MICHALSZARGUT_TASK_REMOVING_LOCAL_FILE]',
                new \Exception("File $csvPath can't be removed"));
    }
}