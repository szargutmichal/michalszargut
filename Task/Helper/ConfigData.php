<?php

namespace MichalSzargut\Task\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Store\Model\ScopeInterface;

class ConfigData extends AbstractHelper
{
    /**
     * @param string $code
     * @param int|null $storeId
     * @return string|null
     */
    public function getGeneralConfig(string $code, ?int $storeId = null): ?string
    {
        return $this->getConfigValue('michalszargut_task/general/' . $code, $storeId);
    }

    /**
     * @param $field
     * @param int|null $storeId
     * @return string|null
     */
    private function getConfigValue(string $field, ?int $storeId): ?string
    {
        return $this->scopeConfig->getValue(
            $field, ScopeInterface::SCOPE_STORE, $storeId
        );
    }

}