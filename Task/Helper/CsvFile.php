<?php
/**
 * Created by PhpStorm.
 * User: szarg
 * Date: 19.02.2019
 * Time: 21:20
 */

namespace MichalSzargut\Task\Helper;

use Magento\Framework\File\Csv;
use MichalSzargut\Task\Logger\Logger;

class CsvFile
{
    /**
     * @var Csv
     */
    private $csvProcessor;
    /**
     * @var Logger
     */
    private $logger;

    public function __construct(
        Csv $csvProcessor,
        Logger $logger
    )
    {
        $this->csvProcessor = $csvProcessor;
        $this->logger = $logger;
    }

    /**
     * @param string $path
     * @return array|null
     */
    public function readCsvFile(string $path): ?array
    {
        try{
            return $this->getData($path);
        }catch (\Exception $ex){
            $this->logger->customError('[MICHALSZARGUT_TASK_HELPER_CSV_READER]', $ex);
            return null;
        }
    }

    /**
     * @param string $path
     * @return bool
     */
    public function removeCsvFile(string $path): bool
    {
        return unlink($path);
    }

    /**
     * @param string $file
     * @param int $length
     * @param string $delimeter
     * @param string $enclosure
     * @return array|null
     * @throws \Exception
     */
    private function getData(string $file, $length = 0, $delimeter = ",", $enclosure = "\""): array
    {
        $data = [];
        if (!file_exists($file)) {
            throw new \Exception('File "' . $file . '" does not exist');
        }

        $fc = fopen($file, 'r');
        $head = fgetcsv($fc, $length, $delimeter, $enclosure);

        while($column = fgetcsv($fc, $length, $delimeter, $enclosure))
        {
            if($column !== [null]){
                if(count($head) === count($column)){
                    $data[] = array_combine($head, $column);
                }else{
                    throw new \Exception("CSV content is invalid");
                }
            }
        }

        fclose($fc);
        return $data;
    }

}