<?php

namespace MichalSzargut\Task\Helper;


use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\Filesystem;
use Magento\Framework\HTTP\Client\Curl;
use MichalSzargut\Task\Logger\Logger;

class FileDownloader
{
    const DIR_PATH = 'simple_products_import';
    /**
     * @var Filesystem
     */
    private $filesystem;
    /**
     * @var Curl
     */
    private $curl;
    /**
     * @var Logger
     */
    private $logger;

    /**
     * FileDownloader constructor.
     * @param Filesystem $filesystem
     * @param Curl $curl
     * @param Logger $logger
     */
    public function __construct(
        Filesystem $filesystem,
        Curl $curl,
        Logger $logger
    )
    {
        $this->filesystem = $filesystem;
        $this->curl = $curl;
        $this->logger = $logger;
    }

    /**
     * @param string $url
     * @return string|null
     */
    public function downloadFile(string $url): ?string
    {
        try {
            $content = $this->getContent($url);

            $mediaCatalog = $this->filesystem
                ->getDirectoryWrite(DirectoryList::MEDIA);
            $fileExtension = explode('.', $url);

            $fileName = self::DIR_PATH . '/import_' . (new \DateTime())->format('YmdHis') . '.' . end($fileExtension);

            if ($mediaCatalog->writeFile($fileName, $content) > 0) {
                return $mediaCatalog->getAbsolutePath().$fileName;
            } else {
                throw new \Exception('Function can\'t save file or file is empty');
            }
        } catch (\Exception $ex) {
            $this->logger->customError('[MICHALSZARGUT_TASK_HELPER_FILE_DOWNLOADER]', $ex);
            return null;
        }
    }

    /**
     * @param string $url
     * @return string|null
     */
    private function getContent(string &$url): ?string
    {
        $this->curl->get($url);

        return $this->curl->getBody();
    }
}