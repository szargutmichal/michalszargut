<?php
/**
 * Created by PhpStorm.
 * User: szarg
 * Date: 21.02.2019
 * Time: 12:30
 */

namespace MichalSzargut\Task\Helper\Product;

use Magento\Eav\Api\AttributeSetRepositoryInterface;
use Magento\Eav\Model\Entity\Attribute\Set;
use Magento\Framework\Api\SearchCriteriaBuilder;

class AttributeSet
{
    /**
     * @var AttributeSetRepositoryInterface
     */
    private $attributeSetRepository;
    /**
     * @var SearchCriteriaBuilder
     */
    private $searchCriteriaBuilder;

    /**
     * AttributeSet constructor.
     * @param AttributeSetRepositoryInterface $attributeSetRepository
     * @param SearchCriteriaBuilder $searchCriteriaBuilder
     */
    public function __construct(
        AttributeSetRepositoryInterface $attributeSetRepository,
        SearchCriteriaBuilder $searchCriteriaBuilder
    )
    {
        $this->attributeSetRepository = $attributeSetRepository;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
    }

    /**
     * @param string $attributeSet
     * @return Set
     */
    public function getAttributeSetByName(string $attributeSet): ?Set
    {
        $searchCriteria = $this->searchCriteriaBuilder
            ->addFilter('attribute_set_name', $attributeSet)
            ->setPageSize(1)
            ->create();
        $attributeSetResult = $this->attributeSetRepository->getList($searchCriteria);

        if ($attributeSetResult->getTotalCount() > 0
            && $attributeSetResult->getTotalCount() === 1
        ) {
            return current($attributeSetResult->getItems());
        } else {
            return null;
        }
    }
}