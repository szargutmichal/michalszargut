<?php
/**
 * Created by PhpStorm.
 * User: szarg
 * Date: 19.02.2019
 * Time: 22:12
 */

namespace MichalSzargut\Task\Helper\Product;


use Magento\Catalog\Model\Product;
use Magento\Catalog\Model\ProductFactory;
use Magento\Catalog\Model\ProductRepository;
use MichalSzargut\Task\Logger\Logger;
use Magento\Store\Model\StoreManagerInterface;
use MichalSzargut\Task\Model\Attribute as ModelAttribute;
use Magento\Catalog\Model\Product\Attribute\Source\Status;

class SimpleProduct
{
    /**
     * @var ProductFactory
     */
    private $productFactory;
    /**
     * @var ProductRepository
     */
    private $productRepository;
    /**
     * @var Logger
     */
    private $logger;

    /**
     * @var int
     */
    private $store;

    const DEFAULT_ATTRIBUTE_SET = 'MichalSzargut_Test';
    /**
     * @var ModelAttribute
     */
    private $attribute;
    /**
     * @var AttributeSet
     */
    private $attributeSetHelper;

    /**
     * SimpleProduct constructor.
     * @param ProductFactory $productFactory
     * @param Logger $logger
     * @param StoreManagerInterface $storeManager
     * @param ProductRepository $productRepository
     * @param ModelAttribute $attribute
     * @param AttributeSet $attributeSetHelper
     */
    public function __construct(
        ProductFactory $productFactory,
        Logger $logger,
        StoreManagerInterface $storeManager,
        ProductRepository $productRepository,
        ModelAttribute $attribute,
        AttributeSet $attributeSetHelper
    )
    {
        $this->productFactory = $productFactory;
        $this->productRepository = $productRepository;
        $this->logger = $logger;
        $this->store = current($storeManager->getStores())->getId();
        $this->attribute = $attribute;
        $this->attributeSetHelper = $attributeSetHelper;
    }

    /**
     * @param array $productsData
     * @return array
     */
    public function createOrUpdateProducts(array &$productsData): array
    {
        $result = ['create' => 0, 'update' => 0, 'fail' => 0];
        $errors = 0;
        foreach ($productsData as $productData) {
            $product = null;
            try {
                if ($product = $this->findProductBySku($productData['sku'])) {
                    $this->saveProduct($productData, $product);
                    $result['update']++;
                } else {
                    $this->saveProduct($productData);
                    $result['create']++;
                }
                $errors = 0;
            } catch (\Exception $ex) {
                $this->logger->customError('[MICHALSZARGUT_TASK_HELPER_SIMPLE_PRODUCT]', $ex);
                $result['fail']++;
                $errors++;
                if ($errors >= 2) {
                    return $result;
                }
            }
        }
        return $result;
    }

    /**
     * @param string $sku
     * @return Product|null
     */
    private function findProductBySku(string &$sku): ?Product
    {
        try {
            return $this->productRepository->get($sku);
        } catch (\Exception $ex) {
            $this->logger->customError('[MICHALSZARGUT_TASK_HELPER_SIMPLE_PRODUCT_FIND]', $ex);
            return null;
        }
    }

    /**
     * @param $productData
     * @param Product|null $product
     * @return Product
     * @throws \Magento\Framework\Exception\CouldNotSaveException
     * @throws \Magento\Framework\Exception\InputException
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Magento\Framework\Exception\StateException
     * @throws \Exception
     */
    private function saveProduct($productData, Product $product = null): Product
    {
        if (!$product) {
            $product = $this->productFactory->create()
                ->setStatus(Status::STATUS_DISABLED);
        }

        $product->setSku($productData['sku'])
            ->setName($productData['name'])
            ->setPrice($productData['price'])
            ->setStoreId($this->store);

            if($attributeSet = $this->attributeSetHelper
                ->getAttributeSetByName(self::DEFAULT_ATTRIBUTE_SET)){
                $product->setAttributeSetId($attributeSet->getAttributeSetId());
            }else{
                throw new \Exception('Required default attribute set with id '
                    .self::DEFAULT_ATTRIBUTE_SET.' does not exist. Cannot set attributes to product '
                    .$productData['sku']);
            }


        if ($attributeOption = $this->attribute
            ->getAttributeSwatch('color', $productData['color'])) {
            $product->setData('color', $attributeOption->getValue());
        }

        $this->productRepository->save($product);

        return $product;

    }
}