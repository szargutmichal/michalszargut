<?php

namespace MichalSzargut\Task\Logger;

use Monolog\Logger;
use Magento\Framework\Logger\Handler\Base;

class Handler extends Base
{
    /**
     * @var int
     */
    protected $loggerType = Logger::ERROR;
    /**
     * @var string
     */
    protected $fileName = '/var/log/import.log';
}