<?php

namespace MichalSzargut\Task\Logger;

class Logger extends \Monolog\Logger
{
    /**
     * @param $message
     * @param \Exception $exception
     * @return bool
     */
    public function customError($message, \Exception $exception)
    {
        $context = [
            'message' => $exception->getMessage(),
            'file' => $exception->getFile(),
            'line' => $exception->getLine(),
            'trace' => $exception->getTraceAsString()
        ];
        return $this->error($message, $context);
    }
}