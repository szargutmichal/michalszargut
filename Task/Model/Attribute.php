<?php
/**
 * Created by PhpStorm.
 * User: szarg
 * Date: 21.02.2019
 * Time: 10:17
 */

namespace MichalSzargut\Task\Model;

use Magento\Eav\Model\Config as ModelConfig;
use Magento\Eav\Api\Data\AttributeOptionInterfaceFactory;
use Magento\Catalog\Api\ProductAttributeOptionManagementInterface;
use Magento\Framework\Model\AbstractModel;

class Attribute extends AbstractModel
{
    protected $eavConfig;
    protected $attributeOptionInterfaceFactory;
    protected $attributeOptionManagement;

    public function __construct(
        ModelConfig $eavConfig,
        AttributeOptionInterfaceFactory $attributeOptionInterfaceFactory,
        ProductAttributeOptionManagementInterface $attributeOptionManagement
    )
    {

        $this->eavConfig = $eavConfig;
        $this->attributeOptionInterfaceFactory = $attributeOptionInterfaceFactory;;
        $this->attributeOptionManagement = $attributeOptionManagement;
    }

    /**
     * @param $attributeCode
     * @param $optionSwatch
     * @return \Magento\Eav\Api\Data\AttributeOptionInterface
     * @throws \Magento\Framework\Exception\InputException
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Magento\Framework\Exception\StateException
     */
    public function getAttributeSwatch($attributeCode, $optionSwatch)
    {
        $optionSwatch = ucfirst($optionSwatch);
        $attribute = $this->eavConfig->getAttribute(\Magento\Catalog\Model\Product::ENTITY, $attributeCode);
        foreach ($attribute->getOptions() as $option) {
            if ($option->getLabel() === $optionSwatch) {
                return $option;
            }
        }
        return $this->registerOption($attribute->getId(), $optionSwatch);
    }

    /**
     * @param $attributeCode
     * @param $label
     * @return \Magento\Eav\Api\Data\AttributeOptionInterface
     * @throws \Magento\Framework\Exception\InputException
     * @throws \Magento\Framework\Exception\StateException
     */
    private function registerOption($attributeCode, $label)
    {
        $option = $this->attributeOptionInterfaceFactory->create();
        $option->setLabel($label)
            ->setValue(strtolower($label));

        $this->attributeOptionManagement->add(
            $attributeCode,
            $option
        );
        return $option;
    }
}