<?php
/**
 * Created by PhpStorm.
 * User: szarg
 * Date: 21.02.2019
 * Time: 12:13
 */

namespace MichalSzargut\Task\Setup;

use Magento\Eav\Model\AttributeSetRepository;
use Magento\Eav\Setup\EavSetupFactory;
use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use MichalSzargut\Task\Helper\Product\AttributeSet;
use MichalSzargut\Task\Helper\Product\SimpleProduct;
use Magento\Catalog\Setup\CategorySetupFactory;
use Magento\Eav\Model\Entity\Attribute\SetFactory as AttributeSetFactory;
use Magento\Eav\Model\Config;
use Magento\Catalog\Model\Product\Type;

class InstallData implements InstallDataInterface
{
    /**
     * @var EavSetupFactory
     */
    private $eavSetupFactory;
    /**
     * @var AttributeSet
     */
    private $attributeSetHelper;
    /**
     * @var AttributeSetFactory
     */
    private $attributeSetFactory;
    /**
     * @var CategorySetupFactory
     */
    private $categorySetupFactory;
    /**
     * @var AttributeSetRepository
     */
    private $attributeSetRepository;
    /**
     * @var Config
     */
    private $eavConfig;

    /**
     * InstallData constructor.
     * @param EavSetupFactory $eavSetupFactory
     * @param AttributeSet $attributeSetHelper
     * @param AttributeSetFactory $attributeSetFactory
     * @param CategorySetupFactory $categorySetupFactory
     * @param AttributeSetRepository $attributeSetRepository
     * @param Config $eavConfig
     */
    public function __construct(
        EavSetupFactory $eavSetupFactory,
        AttributeSet $attributeSetHelper,
        AttributeSetFactory $attributeSetFactory,
        CategorySetupFactory $categorySetupFactory,
        AttributeSetRepository $attributeSetRepository,
        Config $eavConfig
    )
    {
        $this->eavSetupFactory = $eavSetupFactory;
        $this->attributeSetHelper = $attributeSetHelper;
        $this->attributeSetFactory = $attributeSetFactory;
        $this->categorySetupFactory = $categorySetupFactory;
        $this->attributeSetRepository = $attributeSetRepository;
        $this->eavConfig = $eavConfig;
    }

    /**
     * @param ModuleDataSetupInterface $setup
     * @param ModuleContextInterface $context
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();

        $attributeSet = $this->attributeSetHelper->getAttributeSetByName(SimpleProduct::DEFAULT_ATTRIBUTE_SET);
        if(!$attributeSet){
            $categorySetup = $this->categorySetupFactory->create(['setup' => $setup]);

            $attributeSet = $this->attributeSetFactory->create();
            $entityTypeId = $categorySetup->getEntityTypeId(\Magento\Catalog\Model\Product::ENTITY);
            $attributeSetId = $categorySetup->getDefaultAttributeSetId($entityTypeId);
            $data = [
                'attribute_set_name' => SimpleProduct::DEFAULT_ATTRIBUTE_SET,
                'entity_type_id' => $entityTypeId
            ];
            $attributeSet->setData($data);
            $attributeSet->validate();
            $this->attributeSetRepository->save($attributeSet);
            $attributeSet->initFromSkeleton($attributeSetId);
            $this->attributeSetRepository->save($attributeSet);
        }

        $attribute = $this->eavConfig->getAttribute(\Magento\Catalog\Model\Product::ENTITY,'color');
        $eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);
        if($attribute){
            $eavSetup->addAttribute(
                \Magento\Catalog\Model\Product::ENTITY,
                'color',
                [
                    'type' => 'int',
                    'label' => 'Color',
                    'input' => 'select',
                    'required' => false,
                    'user_defined' => true,
                    'apply_to' => implode(',', [Type::TYPE_SIMPLE, Type::TYPE_VIRTUAL]),
                    'is_used_in_grid' => true,
                    'is_visible_in_grid' => false,
                    'attribute_set_id' => $attributeSet->getAttributeSetName()
                ]
            );
        }else{
            $eavSetup->addAttribute(
                \Magento\Catalog\Model\Product::ENTITY,
                'color',
                [
                    'type' => 'int',
                    'label' => 'Color',
                    'input' => 'select',
                    'required' => false,
                    'user_defined' => true,
                    'apply_to' => implode(',', [Type::TYPE_SIMPLE, Type::TYPE_VIRTUAL]),
                    'is_used_in_grid' => true,
                    'is_visible_in_grid' => false,
                    'attribute_set_id' => $attributeSet->getAttributeSetName()
                ]
            );
        }



        $setup->endSetup();
    }
}